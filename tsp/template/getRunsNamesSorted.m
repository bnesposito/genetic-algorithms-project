function output = getRunsNamesSorted(fitness, runsName)
output = runsName;
output(:,5) = fitness;
output(:,6) = 1:size(fitness);
[~,idx] = sort(output(:,5)); 
output = output(idx,:);
end