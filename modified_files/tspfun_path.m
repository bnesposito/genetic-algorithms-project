% calculate the TSP fitness function for path representation

function ObjVal = tspfun_path(Phen, Dist)
n_points = size(Phen, 2);
ObjVal = [];
for ind = 1:size(Phen, 1) 
    ObjVal(ind)=Dist(Phen(ind,n_points),Phen(ind,1));
    for t=1:n_points-1
        new_dist = Dist(Phen(ind,t),Phen(ind,t+1));
        ObjVal(ind)=ObjVal(ind)+new_dist;
    end
ObjVal = ObjVal';
end
