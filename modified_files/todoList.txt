Question2
*(DONE) Modify NVAR such that it reflects the number of cities. 
*(DONE) Add number of generations as hyperparameter 
*(DONE) Try more values for the hyperparameters
*(DONE) Check which minimum that algorithm is taking. Select the minimum across all generations.
*(DONE) Performance criteria: fitness given certain number of number of generations.
*(DISMISSED) Report computational time. 
* Generate phenotype plot of the best instance for each map.
* Mention that it is clear that there is room for improvement with more generations for map 3.
* Mention computational limitations
* Mention that the criteria is best fitness (smallest) for 500 generations.
* Mention that the criteria to select the optimal instance is the sum of the fitness. This penalizes more
  instances that do not fit well. This is a desirable property. 

Question 3
*(DONE) Stopping criteria: small diversity in the population and small change in the fitness of the mean/best. Combination?
*(DONE) Run and plot for selected instances
* Mention that, by looking at the trace of the best instances, we are assuming that the results from 2 hold for the
  stopping criterion. We do not observe cases where there is a local optima and we get out of it (stair behaviour).

Question 4
*(DONE) Check the different representations for the TSP. Path vs adjacency. Pag 67 of the book. 
*(DONE) Function to evaluate fitness for path representation.
*(DONE) Does the selection function have to change?
*(DONE) Inverse mutation for adjacency representation. (different inverse mutation for each rep)
*(DONE) Appropriate strategy for combination?
* Confirm that alternating edge crossover (adjencency representation) is the same as edge recombination operator 
  (path representation). It is not. I think we should implement an appropiate crossover operator.
* Meter en run_ga_modified (y en general en todo) lo relacionado a adj_ref 

Question 5
* (DISMISSED) Pag 91 of the book. Multimodal problems. 
* (DISMISSED) Reduction of the variance of the results? Perhaps we also gain fitness. Check negative consequences of 
  this strategy, increased computational time. 
* (DONE) Preguntar tamas
* Check if LOCALLOOP is related to 2-opt method.
* This makes convergence a lot faster, but the same result can be achieved with more generations.

Question 6 
* Check that the algorithm selected solves the TSP correctly.
* First Belgian road, because it�s the smallest. 

Question 7
* Pick based on time.
* (DONE) Question A

