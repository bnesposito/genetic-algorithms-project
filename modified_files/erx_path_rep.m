function offspring = erx_path_rep(parents);

num_cities = size(parents{1},2);

edge_lists = cell(num_cities,1);
for p = 1:2
    for i = 1:num_cities
        
        if i == 1
            prevs = parents{p}(end);
        else
            prevs = parents{p}(i-1);
        end
        
        if i == num_cities
            nxt = parents{p}(1);
        else
            nxt = parents{p}(i+1);
        end
        
        edge_lists{parents{p}(i)} = unique([edge_lists{parents{p}(i)}, ...
            prevs, ...
            nxt]);
    end
end

current_city = parents{1}(1);
offspring = [current_city];
remaining_cities = setdiff(1:num_cities, current_city);

while size(remaining_cities, 2) ~= 0
    
    % erase current city from edge list
    for i = 1:num_cities
        edge_lists{i} = setdiff(edge_lists{i}, current_city);
    end
    
    % select next city
    cities_edge_current = edge_lists{current_city};
    if size(cities_edge_current,2) > 0
        num_edges = [];
        counter = 0;
        for j = cities_edge_current
            counter = counter + 1;
            num_edges(counter) = size(edge_lists{j},2);
        end
        [min_value, min_idx] = min(num_edges);
        cities_min_val_idx = num_edges == min_value;
        cities_min_val = cities_edge_current(cities_min_val_idx);
        num_competing_cities = size(cities_min_val,2);
        
        % if more than 1 city has the lowest amount of edges
        if num_competing_cities > 1
            rand_perm = randperm(num_competing_cities);
            current_city = cities_min_val(rand_perm(1));
        else
            current_city = cities_min_val;
        end
    else
        rand_perm = randperm(size(remaining_cities,2));
        current_city = remaining_cities(rand_perm(1));
    end
    offspring = [offspring, current_city];
    remaining_cities = setdiff(remaining_cities, current_city);
end
        
            
            
            
            
