function NewChrIx = tournament_sel(FitnV,Nsel,k)

% Identify the population size (Nind)
   [Nind,ans] = size(FitnV);
   
NewChrIx = zeros(Nsel,1);

% Perform tournament selection
for i = 1:Nsel
    partic = randi([1, Nind], k, 1);
    partic_fits = FitnV(partic);
    [minv, mini] = min(partic_fits);
    ind_min_val = partic(partic_fits == minv);
    num_ind_min_val = size(ind_min_val,1);
    if  num_ind_min_val > 1
        rand_perm = randperm(num_ind_min_val);
        sel_ind = ind_min_val(rand_perm(1));
    else
        sel_ind = ind_min_val;
    end
    NewChrIx(i) = sel_ind;
end

end
