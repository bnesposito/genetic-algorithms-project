function results = run_ga_modified(x, y, show_plot,NIND, MAXGEN, NVAR, ELITIST, stopping_gens, stopping_percentage, PR_CROSS, PR_MUT, CROSSOVER, LOCALLOOP, adj_rep, sel_type, k)
% usage: run_ga(x, y,
%               NIND, MAXGEN, NVAR,
%               ELITIST, STOP_PERCENTAGE,
%               PR_CROSS, PR_MUT, CROSSOVER,
%               ah1, ah2, ah3)
%

results = cell(1,4);

if show_plot == true
    % Graphical parameters
    fh = figure('Visible','off','Name','TSP Tool','Position',[0,0,1024,768]);
    ah1 = axes('Parent',fh,'Position',[.1 .55 .4 .4]);
    plot(x,y,'ko')
    ah2 = axes('Parent',fh,'Position',[.55 .55 .4 .4]);
    axes(ah2);
    xlabel('Generation');
    ylabel('Distance (Min. - Gem. - Max.)');
    ah3 = axes('Parent',fh,'Position',[.1 .1 .4 .4]);
    axes(ah3);
    title('Histogram');
    xlabel('Distance');
    ylabel('Number');
end

%{NIND MAXGEN NVAR ELITIST STOP_PERCENTAGE PR_CROSS PR_MUT CROSSOVER LOCALLOOP}


GGAP = 1 - ELITIST;
mean_fits=zeros(1,MAXGEN+1);
worst=zeros(1,MAXGEN+1);
Dist=zeros(NVAR,NVAR);
for i=1:size(x,1)
    for j=1:size(y,1)
        Dist(i,j)=sqrt((x(i)-x(j))^2+(y(i)-y(j))^2);
    end
end

% initialize population
Chrom=zeros(NIND,NVAR);

for row=1:NIND
    
    % use appropriate representation
    if adj_rep
        Chrom(row,:)=path2adj(randperm(NVAR));
    else
        Chrom(row,:)=randperm(NVAR);
    end
    
end

gen=0;

% stopping criteria variables
gens_no_improv = 0;
stopping_gens_stop_met = false;
percentage_gens_stop_met = false;
stopping_eq_ind = ceil(stopping_percentage * NIND);
stop_final = false;

% evaluate initial population
if adj_rep
    ObjV = tspfun(Chrom,Dist);
else
    ObjV = tspfun_path(Chrom, Dist);
end

best=zeros(1,MAXGEN);

%% generational loop
while gen<MAXGEN
    
    % sort fitness values
    sObjV=sort(ObjV);
    best(gen+1)=min(ObjV);
    minimum=best(gen+1);
    mean_fits(gen+1)=mean(ObjV);
    worst(gen+1)=max(ObjV);
    
    % store results
    results{1} = minimum;
    results{2} = best;
    results{3} = mean_fits(1:end-1);
    results{4} = [false, false]; % early stopping
    
    for t=1:size(ObjV,1)
        if (ObjV(t)==minimum)
            break;
        end
    end
    
    % plot process
    if show_plot == true
        if adj_rep
            visualizeTSP(x,y,adj2path(Chrom(t,:)), minimum, ah1, gen, best, mean_fits, worst, ah2, ObjV, NIND, ah3);
        else
            visualizeTSP(x,y,Chrom(t,:), minimum, ah1, gen, best, mean_fits, worst, ah2, ObjV, NIND, ah3);
        end
        
    end
    
    % select individuals for breeding
    if strcmp(sel_type,'tournament')
        SelCh=select_modified('tournament_sel', Chrom, ObjV, GGAP,1,k);
    elseif strcmp(sel_type,'fps')
        FitnV = ObjV - min(ObjV);
        SelCh=select('sus', Chrom, ObjV, GGAP,1);
    elseif strcmp(sel_type,'ranking')
        FitnV=ranking(ObjV);
        SelCh=select('sus', Chrom, FitnV, GGAP,1);
    end
    
    %recombine individuals (crossover)
    if adj_rep
        representation = 1;
    else
        representation = 2;
    end
    
    SelCh = recombin_modified(CROSSOVER,SelCh,PR_CROSS, 1, representation);
    SelCh=mutateTSP_modified('inversion',SelCh,PR_MUT,representation);
    
    %evaluate offspring, call objective function
    if adj_rep
        ObjVSel = tspfun(SelCh,Dist);
    else
        ObjVSel = tspfun_path(SelCh,Dist);
    end
    
    %reinsert offspring into population
    [Chrom ObjV]=reins(Chrom,SelCh,1,1,ObjV,ObjVSel);
    
    Chrom = tsp_ImprovePopulation_modified(NIND, NVAR, Chrom,LOCALLOOP,Dist, representation);
    
    %% stopping criterias
    
    % by lack of improvement
    if stopping_gens > 0 & gen > 0
        if best(gen+1) + 1e-15 >= best(gen)
            gens_no_improv = gens_no_improv + 1;
        else
            gens_no_improv = 0;
        end
        
        if gens_no_improv >= stopping_gens
            stopping_gens_stop_met = true;
        end
    end
    
    % by lack of diversity
    if stopping_percentage > 0 & gen > 0
        if sObjV(stopping_eq_ind) - sObjV(1) <= 1e-15
            percentage_gens_stop_met = true;
        end
    end
    
    if stopping_gens > 0 & stopping_percentage > 0
        if stopping_gens_stop_met & percentage_gens_stop_met
            results{2} = results{2}(1:gen+1);
            results{3} = results{3}(1:gen+1);
            results{4} = [true, true];
            break;
        end
    elseif stopping_gens_stop_met
        results{2} = results{2}(1:gen+1);
        results{3} = results{3}(1:gen+1);
        results{4} = [true, false];
        break;
    elseif percentage_gens_stop_met
        results{2} = results{2}(1:gen+1);
        results{3} = results{3}(1:gen+1);
        results{4} = [false, true];
        break;
    end
    
    %% increment generation counter
    gen=gen+1;
    
end

end
